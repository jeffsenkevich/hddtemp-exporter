FROM python:alpine

COPY requirements.txt /usr/src/hddtemp-exporter/
COPY hddtemp-exporter.py /usr/src/hddtemp-exporter/

RUN pip install -r /usr/src/hddtemp-exporter/requirements.txt

EXPOSE 9250

ENTRYPOINT ["python", "/usr/src/hddtemp-exporter/hddtemp-exporter.py"]
