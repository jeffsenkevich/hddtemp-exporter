import argparse
import logging
import os
import socket
import sys
import time

from prometheus_client import start_http_server
from prometheus_client.core import GaugeMetricFamily, REGISTRY


class HddTempScraper(object):
    def __init__(self, hddtemp_address):
        parts = hddtemp_address.split(':')
        self._hddtemp_host = parts[0]
        self._hddtemp_port = int(parts[1])

    def scrape(self):
        try:
            with socket.create_connection((self._hddtemp_host, self._hddtemp_port)) as sock:
                resp = b''.join(self._recv_all(sock)).decode(errors='ignore')
                logging.debug('read from socket=%s', resp)
                return self._parse_data(resp)
        except ConnectionRefusedError:
            logging.error('error connecting to hddtemp daemon')
            return []
        except OSError as err:
            logging.error('%s: verify hddtemp address', err)
            sys.exit(1)

    @staticmethod
    def _recv_all(sock, buffer_size=1024):
        buf = sock.recv(buffer_size)
        while buf:
            yield buf
            buf = sock.recv(buffer_size)

    @staticmethod
    def _parse_data(data):
        data = data.strip('|')
        return [hdd.split('|') for hdd in data.split('||')]


class HddTempCollector(object):
    METRIC_NAME = 'sensor_hddsmart_temperature_celsius'
    METRIC_DESC = 'HDD temperature in celcius'

    def __init__(self, hddtemp_address):
        self._hddtemp_scraper = HddTempScraper(hddtemp_address)

    def collect(self):
        hdd_temps = self._hddtemp_scraper.scrape()

        metric = GaugeMetricFamily(self.METRIC_NAME, self.METRIC_DESC, labels=['device', 'id'])

        for hdd_temp in hdd_temps:
            logging.debug('hdd_temp=%s', hdd_temp)
            labels = hdd_temp[:2]
            if hdd_temp[2] != 'UNK':
                metric.add_metric(labels, int(hdd_temp[2]))

        yield metric


def parse_args():
    parser = argparse.ArgumentParser(description='HDD temperature exporter')

    parser.add_argument('--port', metavar='port', type=int,
                        help='Port for exporter to listen on',
                        default=int(os.environ.get('PORT', '9250')))

    parser.add_argument('--hddtemp-address', metavar='hddtemp_address', type=str,
                        help='Host and port that the hddtemp daemon is running on',
                        default=os.environ.get('HDDTEMP_ADDRESS', 'localhost:7634'))

    parser.add_argument('--debug', dest='debug', action='store_true',
                        help='Debug mode',
                        default=bool(os.environ.get('DEBUG', False)))

    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()

    if args.debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    # Register custom collector
    REGISTRY.register(HddTempCollector(args.hddtemp_address))

    # Start up the server to expose the metrics.
    logging.info("starting exporter on port %d", args.port)
    start_http_server(args.port)

    while True:
        time.sleep(1)
